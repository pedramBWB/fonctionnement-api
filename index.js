const { default: axios } = require('axios')
const express = require('express')
const res = require('express/lib/response')
const app = express()
var bodyParser = require('body-parser')
const req = require('express/lib/request')
const port = 7777

// Installer via npm : Express, Axios, Body-parser, cors et jsonwebtoken


app.use(bodyParser.json)

// pour chaque requête, on renvoie l'en-tête au serveur d'authentification pour vérifier les infos
// Permet de sécuriser la naviguation entre les pages
app.use((req, res, next)=> {

    if (req.path !== '/authorize')
    axios.post("172.17.0.1:5555/verification", req.headers)
        .then(response => next())
        .catch(error => res.status(403).send("unauthorized"));
})

app.get('/', (req, res) => {
  res.send('API is working')
})

// Renvoie une reponse avec les infos client vers le serveur d'authentification
app.post('/authorize', (req, res) => {

    // récupère les informations envoyé par le client
    let payload = req.body;

    // requête post via AXIOS (https://github.com/axios/axios)
    axios.post("172.17.0.1:5555/authorize", payload)
        .then((response)=> {
            res.status(200).json(response.data);
        })
        .catch(error => res.status(403).send("unauthorized"))
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})